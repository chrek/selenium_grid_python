import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

import time

driver = webdriver.Chrome()

# Navigate to a website
driver.get("https://github.com")
# Delay execution for 5 seconds to view the maximize operation
time.sleep(5)
# Mazimize current window
driver.maximize_window()

# Delay execution for 5 seconds to view the maximize operation
time.sleep(5)

# Verify title
print(driver.title)
assert "GitHub" in driver.title
elem = driver.find_element_by_name("q")
elem.clear()
elem.send_keys("test is going on")
elem.send_keys(Keys.RETURN)
assert "No results found." not in driver.page_source

# Close the browser
# driver.close()
driver.quit()
