import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

import time

# driver = webdriver.Chrome()

driver = webdriver.Remote(
    command_executor='http://192.168.56.1:4444/wd/hub',
    desired_capabilities={'browserName': 'firefox', 'javascriptEnabled': True})

# Navigate to a website
driver.get("https://github.com")
# Delay execution for 5 seconds to view the maximize operation
time.sleep(5)
# Mazimize current window
driver.maximize_window()

# Delay execution for 5 seconds to view the maximize operation
time.sleep(5)

# Verify title
print(driver.title)
assert "GitHub" in driver.title
elem = driver.find_element_by_name("q")
elem.clear()
elem.send_keys("test is going on")
elem.send_keys(Keys.RETURN)
assert "No results found." not in driver.page_source

# Close the browser
# driver.close()
driver.quit()
